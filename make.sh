#!/bin/bash
start=$(cat status.current)
end=`expr $(cat status.current) + 100`
verbose=0
logoB=0
logoPath="logo";
headB=0
footB=0
head=""
foot=""

function generate {
  custEcho  "#-   START GENERATE    -#";
  if [ ${verbose} -eq 1 ]
  then
    pdflatex --shell-escape --interaction=nonstopmode Barcodes.tex
  else
    pdflatex --shell-escape --interaction=nonstopmode Barcodes.tex >/dev/null 2>/dev/null
  fi

  custEcho  "#-   END GENERATE    -#";
}

function cleanup {
  custEcho  "#-   START CLEANUP    -#";
  rm -f Barcodes.ps
  rm -f Barcodes.aux
  rm -f Barcodes.bbl
  rm -f Barcodes.bit
  rm -f Barcodes.blg
  rm -f Barcodes.dvi
  rm -f Barcodes.glo
  rm -f Barcodes.idx
  rm -f Barcodes.ilg
  rm -f Barcodes.ilg
  rm -f Barcodes.ind
  rm -f Barcodes.lof
  rm -f Barcodes.log
  rm -f Barcodes.lot
  rm -f Barcodes.nlo
  rm -f Barcodes.nls
  rm -f Barcodes.out
  rm -f Barcodes.toc
  rm -f Barcodes.synctex.gz
  rm -f Barcodes-pics.pdf
  custEcho "#-   END CLEANUP!    -#";
}

function setConfig {
  echo "" > BarcodesConfig.tex
  echo "\setcounter{start}{${start}} %% bei welchem barcode soll mit dem generieren begonnen werden" >> BarcodesConfig.tex
  echo "\setcounter{stop}{${end}} %% bis inklusive welchem barcode soll generiert werden" >> BarcodesConfig.tex
  if [ ${headB} -eq 1 ]
  then
    echo "\newcommand{\headString}{${head}} %% auskommentieren wenn header nicht gewuenscht; der parameter gibt den einzufuehgenden text an" >> BarcodesConfig.tex
  else
    echo "%\newcommand{\headString}{${head}} %% auskommentieren wenn header nicht gewuenscht; der parameter gibt den einzufuehgenden text an" >> BarcodesConfig.tex
  fi
  if [ ${footB} -eq 1 ]
  then
    echo "\newcommand{\footString}{${foot}} %% auskommentieren wenn foot nicht gewuenscht; der parameter gibt den einzufuehgenden text an" >> BarcodesConfig.tex
  else
    echo "%\newcommand{\footString}{${foot}} %% auskommentieren wenn foot nicht gewuenscht; der parameter gibt den einzufuehgenden text an" >> BarcodesConfig.tex
  fi
  if [ ${logoB} -eq 1 ]
  then
    echo "\newcommand{\includeBackside}{${logoPath}} %% auskommentieren wenn backside nicht gewuenscht; der parameter gibt die einzubindende datei an" >> BarcodesConfig.tex
  else
    echo "%\newcommand{\includeBackside}{${logoPath}} %% auskommentieren wenn backside nicht gewuenscht; der parameter gibt die einzubindende datei an" >> BarcodesConfig.tex
  fi
  }

function custEcho {
  if [ ${verbose} -eq 1 ]
  then
    echo $1;
  fi
}

function printHelp {
  echo "Options:"
  echo -e "\t-s ARG : sets the first barcode to be generated to ARG: default status.current aka save file from last time"
  echo -e "\t-e ARG : sets the last barcode to be generated to ARG; default start + 100"
  echo -e "\t-g ARG : will generate ARG barcodes; default 100"
  echo -e "\t-l : enables Backside with default logo"
  echo -e "\t-L ARG : enables Backside and sets the logo to ARG;default \"logo\""
  echo -e "\t-v : enables output; default off"
  echo -e "\t-h ARG : enables the header and sets it to ARG; default off"
  echo -e "\t-f ARG : enables the footer and sets it to ARG; default off"
  echo "Commands:"
  echo -e "\tgenerate : generate and step up the counter"
  echo -e "\tsimulate : simulates a generation aka does not step up the counter"
  echo -e "\tsetConfig : only sets the config file to the given options"
  echo -e "\tstatus : just prints the first new barcode (last + 1)"
}

while getopts ":s:e:g:lL:vh:f:" opt
do
	case ${opt} in
    s) #default status.current
      start=${OPTARG}
      ;;
    e) #defalt s+100
      end=${OPTARG}
      ;;
    g) #default 100
      end=`${start} + ${OPTARG}`
      ;;
    l) #default 0
      logoB=1
      ;;
    L) #default "logo"
      logoPath=${OPTARG}
      ;;
    v) #default 0
      verbose=1
      ;;
    h) #default ""
      headB=1
      head=${OPTARG}
      ;;
    f) #default ""
      footB=1
      foot=${OPTARG}
        ;;
    \?)
  		echo "Error: Invalid option \"-$OPTARG\""
  		printHelp
  		;;
  	:)
  		echo "Error: Option \"-$OPTARG\" needs a parameter"
  		printHelp
  		;;
  esac
done

case ${@: -1} in
  generate)
    setConfig
    generate
    expr $end + 1 > status.current # save last id
    cleanup
  ;;
  simulate)
    setConfig
    generate
    cleanup
  ;;
  setConfig)
    setConfig
  ;;
  status)
    cat status.current
  ;;
  *)
    echo "Usage: $0 {-s|-e|-g|-l|-L|-v|-h|-f} {generate|simulate|setConfig|status}"
    printHelp
  ;;
esac
